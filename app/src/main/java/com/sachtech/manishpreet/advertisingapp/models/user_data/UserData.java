package com.sachtech.manishpreet.advertisingapp.models.user_data;

import com.google.gson.annotations.SerializedName;


public class UserData {

    @SerializedName("Status")
    private boolean status;

    @SerializedName("Message")
    private String message;

    @SerializedName("data")
    private Data data;

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return
                "UserData{" +
                        "status = '" + status + '\'' +
                        ",message = '" + message + '\'' +
                        ",data = '" + data + '\'' +
                        "}";
    }
}