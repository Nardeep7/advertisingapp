package com.sachtech.manishpreet.advertisingapp.base

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.sachtech.manishpreet.advertisingapp.R
import timetracker.sts.jainagdev.parkingapp.navigator.Navigator
import kotlin.reflect.KClass


/**
 * * Created by Gurtek Singh on 1/1/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

open class KotlinBaseActivity(@IdRes private val container: Int = 0) : FragmentActivity(), KotlinBaseListener {


    fun longToast(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show()
    }

    fun switchFragment(container: ViewGroup, fragment: Class<Fragment>, isAddTobackStack: Boolean) {


    }


    fun shortToast(msg: String) {
        Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
    }


    open fun addDialog(resId: Int): Dialog {
        val d = Dialog(this)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        d.setCancelable(true)
        d.setContentView(resId)
        d.window!!
            .setBackgroundDrawableResource(android.R.color.transparent)
        return d

    }




    override fun openAForResult(kClass: KClass<out AppCompatActivity>, bundle: Bundle, code: Int) {
        navigator.openAForResult(kClass, bundle, code)
    }

    override fun navigateToFragment(java: KClass<out Fragment>, extras: Bundle?) {
        navigateToFragment(java.java, extras, transitionView = null)
    }

    override fun addFragment(fragment: KClass<out Fragment>, extras: Bundle?, tag: String) {
        navigator.addFragment(fragment.java, tag = tag, bundle = extras)
    }

    private val manager: FragmentManager by lazy {
        supportFragmentManager
    }

    private val navigator: Navigator by lazy { Navigator(this, container) }


    fun navigateToFragment(clazz: Class<out Fragment>, bundle: Bundle? = null, transitionView: View? = null) {
        navigator.replaceFragment(clazz, bundle, transitionView)
    }

    fun switchFragment(clazz: Class<out Fragment>, bundle: Bundle? = null, transitionView: View? = null) {
        navigator.addFragment(clazz, bundle, transitionView)
    }


    override fun openActivity(kClass: KClass<out AppCompatActivity>, extras: Bundle?) {
        navigator.openA(kClass, extras)
    }

    override fun onBackPressed() {
        if (manager.backStackEntryCount == 1) finish()
        else super.onBackPressed()
    }

    /*fun currentVisibleFragmentTag(): String? {
        return navigator.getCurrentFragmentTag()
    }*/

    inline fun <reified T : Fragment> getFragment(): T? {
        val fragment = supportFragmentManager.findFragmentByTag(T::class.java.simpleName)
        return if (fragment != null) fragment as T else null
    }


    override fun addChildFragment(childFragmentManager: FragmentManager, container: Int, kClass: KClass<out Fragment>) {
        navigator.addChildFragment(childFragmentManager, container, kClass)
    }

    fun bringtoFront(kClass: KClass<out Fragment>) {
        navigator.bringFragmentToFrontIfPresentOrCreate(kClass.java)
    }

    fun <T> showDialog(clazz: Class<out KotlinBaseDialogFragment<T>>, bundle: Bundle? = Bundle()): Fragment {
        val tag = clazz.simpleName
        val ft = supportFragmentManager?.beginTransaction()
        var fragment = supportFragmentManager?.findFragmentByTag(tag)
        if (fragment != null) {
            ft?.remove(fragment)
        }
        ft?.addToBackStack(tag)

        // Create and show the dialog.
        fragment = clazz.newInstance()
        fragment.arguments = bundle
        fragment.show(ft, tag)

        return fragment
    }

    open fun showDialog() {
        if (dialog.isShowing)
            dialog.dismiss()
        try {
            dialog.show()
        } catch (e: Exception) {

        }

    }

    open fun storeData() {

    }

    open fun hideDialog() {
        if (dialog != null) {
            if (dialog.isShowing)
                dialog.dismiss()
        }
    }

    private val dialog by lazy {
        Dialog(this, R.style.CustomDialog).apply {
            window.setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            setCancelable(false)
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(R.layout.progress_dialog)
            //findViewById<RotateLoading>(R.uid.progress).start()
        }
    }


}
