package com.sachtech.manishpreet.advertisingapp.ui

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.balwinderbadgal.vidpro.intracter.ApiHitterHelper
import com.balwinderbadgal.vidpro.intracter.DataGetter
import com.google.gson.Gson
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.base.KotlinBaseFragment
import com.sachtech.manishpreet.advertisingapp.extension.openActivity
import com.sachtech.manishpreet.advertisingapp.extension.setprefrenceObject
import com.sachtech.manishpreet.advertisingapp.extension.toast
import com.sachtech.manishpreet.advertisingapp.keys.PrefrencesKeys
import com.sachtech.manishpreet.advertisingapp.models.user_data.UserData
import com.sachtech.manishpreet.advertisingapp.ui.video_player.MainActivity
import kotlinx.android.synthetic.main.fragment_register_customer.*
import okhttp3.ResponseBody
import java.util.*

class FragmentRegister_Customer : KotlinBaseFragment(R.layout.fragment_register_customer), DataGetter {

    override fun onError(message: String?) {
        hideDialog()
        toast(message)
    }

    override fun onFetchData(body: ResponseBody?) {
        hideDialog()
        var userdata= Gson().fromJson(body!!.string(), UserData::class.java)
        if (userdata.isStatus)
        {
            toast(userdata.message)
            setprefrenceObject(PrefrencesKeys.LOGED_IN,true)
            setprefrenceObject(PrefrencesKeys.USER,userdata)
            context!!.openActivity<MainActivity>()
            activity!!.finish()
        }else{
            toast(userdata.message)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtAlreadyHaveAccount.setOnClickListener {
            baselistener.navigateToFragment(FragmentLogin::class)
        }
        txtRegister.setOnClickListener {
            makeRegistration()
        }
        ivCalendar.setOnClickListener { openCalendar() }

    }
    private fun openCalendar() {

        // Get Current Date
        val c = Calendar.getInstance()
        var mYear = c.get(Calendar.YEAR)
        var mMonth = c.get(Calendar.MONTH)
        var mDay = c.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(context!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                etSellerBirthdate.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year) },
            mYear,
            mMonth,
            mDay
        )
        datePickerDialog.show()

    }
    private fun makeRegistration() {

        var customerName = etCustomerName.text.toString().trim()
        var customerUser = etCustomerUser.text.toString().trim()
        var email = etCustomerEmail.text.toString().trim()
        var password = etCustomerPassword.text.toString().trim()
        var nif = etCustomerNif.text.toString().trim()
        var cellphone1 = etCustomerCellphone1.text.toString().trim()
        var cellphone2 = etCustomerCellphone2.text.toString().trim()
        var contactName = etContactName.text.toString().trim()
        var customerAddress = etCustomerAddress.text.toString().trim()
        var customerZipcode = etCustomerZipCode.text.toString().trim()
        var customerDistrict = etCustomerDistrict.text.toString().trim()
        var customerConselho = etCustomerConselho.text.toString().trim()
        var customerFreegsia = etCustomerFreguesia.text.toString().trim()
        var customerBranch = etCustomerBranch.text.toString().trim()
        var customerTraffic = etCustomerTraffic.text.toString().trim()
        var customerBirthdate = etSellerBirthdate.text.toString().trim()
        if (isValidate(
                customerName,
                customerUser,
                email,
                password,
                nif,
                cellphone1,
                cellphone2,
                contactName,
                customerAddress,
                customerZipcode,
                customerDistrict,
                customerConselho,
                customerFreegsia,
                customerBranch,
                customerTraffic,
                customerBirthdate
            )
        ) {
            showDialog()
            ApiHitterHelper().registerWithCustomer(
                customerName,
                customerUser,
                email,
                password,
                nif,
                cellphone1,
                cellphone2,
                contactName
                ,
                customerAddress,
                customerZipcode,
                customerDistrict,
                customerConselho,
                customerFreegsia,
                customerBranch,
                customerTraffic,
                customerBirthdate,
                this
            )
        }
    }

    private fun isValidate(
        customerName: String,
        customerUser: String,
        email: String,
        password: String,
        nif: String,
        cellphone1: String,
        cellphone2: String,
        contactName: String,
        customerAddress: String,
        customerZipcode: String,
        customerDistrict: String,
        customerConselho: String,
        customerFreegsia: String,
        customerBranch: String,
        customerTraffic: String,
        sellerBirthdate: String
    ): Boolean {

        if (customerName.isEmpty() || customerUser.isEmpty() || email.isEmpty() || password.isEmpty() || nif.isEmpty() || cellphone1.isEmpty() || cellphone2.isEmpty() ||
            contactName.isEmpty() || customerAddress.isEmpty() || customerZipcode.isEmpty() || customerDistrict.isEmpty() || customerConselho.isEmpty() ||
            customerFreegsia.isEmpty() || customerBranch.isEmpty() || customerTraffic.isEmpty()||sellerBirthdate.isEmpty()
        ) {

            toast("All field required")
            return false

        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                toast("Enter valid Email")
                return false
            } else
                return true
        }

    }
}