package com.sachtech.manishpreet.advertisingapp.extension

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Patterns
import android.widget.Toast
import com.sachtech.manishpreet.advertisingapp.app.App

fun toast(text: String?) {
    Toast.makeText(App.application, text, Toast.LENGTH_LONG).show()
}

fun Array<String>.withPermissions(activity: Activity, body: (() -> Unit)? = null) {
    if (hasPermissions(this)) {
        body?.invoke()
    } else {
        askPermissions(activity)
        checkingPermissionsStatus(activity, body)
    }
}

private fun Array<String>.askPermissions(activity: Activity) {
    ActivityCompat.requestPermissions(activity,
        this,
        700)
}

private fun Array<String>.checkingPermissionsStatus(activity: Activity, body: (() -> Unit)?) {
    val maxCount = 20
    var count = 0

    val handler = Handler()
    var runnable: Runnable? = null
    runnable = Runnable {
        if (hasPermissions(this)) {
            body?.invoke()
            handler.removeCallbacksAndMessages(null)
            runnable = null
        }
        if (count > maxCount) {
            handler.removeCallbacksAndMessages(null)
            runnable = null
        }
        count += 1
        handler.postDelayed(runnable, 500)
    }
    handler.postDelayed(runnable, 500)
}

fun <T> T.hasPermissions(permissions: Array<String>): Boolean {
    var allPerm = true
    for (permission: String in permissions) {
        if (ContextCompat.checkSelfPermission(App.getApp(), permission)
            != PackageManager.PERMISSION_GRANTED) {
            allPerm = false
        }
    }
    return allPerm
}

inline fun <reified T> Context.openActivity(body: Intent.() -> Unit = {}) {
    val intent = Intent(App.application.applicationContext, T::class.java)
    intent.body()
    startActivity(intent)
}

fun isvalidate(email: String, pass: String): Boolean {
    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        toast("enter valid e-mail")
    else if (pass.length <= 6)
        toast("password should be grater than 6 chracter")
    else
        return true

    return false
}

var dialog: ProgressDialog? = null
fun showDialog() {
    dialog = ProgressDialog(App.application)
    dialog!!.setCancelable(false)
    dialog!!.setCanceledOnTouchOutside(false)
    dialog!!.setMessage("Processing...")
    dialog!!.show()
}

fun hideDialog() {
    if (dialog != null)
        dialog!!.dismiss()
}