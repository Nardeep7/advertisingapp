package com.sachtech.manishpreet.advertisingapp.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.base.KotlinBaseActivity

class LoginActivity : KotlinBaseActivity(R.id.container) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        addFragment(FragmentLogin::class)
    }
}
