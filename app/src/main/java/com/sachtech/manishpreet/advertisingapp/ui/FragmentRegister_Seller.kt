package com.sachtech.manishpreet.advertisingapp.ui

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.balwinderbadgal.vidpro.intracter.ApiHitterHelper
import com.balwinderbadgal.vidpro.intracter.DataGetter
import com.google.gson.Gson
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.base.KotlinBaseFragment
import com.sachtech.manishpreet.advertisingapp.extension.openActivity
import com.sachtech.manishpreet.advertisingapp.extension.setprefrenceObject
import com.sachtech.manishpreet.advertisingapp.extension.toast
import com.sachtech.manishpreet.advertisingapp.keys.PrefrencesKeys
import com.sachtech.manishpreet.advertisingapp.models.user_data.UserData
import com.sachtech.manishpreet.advertisingapp.ui.video_player.MainActivity
import kotlinx.android.synthetic.main.fragment_register_seller.*
import okhttp3.ResponseBody


class FragmentRegister_Seller : KotlinBaseFragment(R.layout.fragment_register_seller), DataGetter {


    override fun onError(message: String?) {
        hideDialog()
        toast(message)
    }

    override fun onFetchData(body: ResponseBody?) {
        hideDialog()
        var userdata= Gson().fromJson(body!!.string(), UserData::class.java)
        if (userdata.isStatus)
        {
            toast(userdata.message)
            setprefrenceObject(PrefrencesKeys.LOGED_IN,true)
            setprefrenceObject(PrefrencesKeys.USER,userdata)
            context!!.openActivity<MainActivity>()
            activity!!.finish()
        }else{
            toast(userdata.message)
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtAlreadyHaveAccount.setOnClickListener {
            baselistener.navigateToFragment(FragmentLogin::class)
        }

        txtRegister.setOnClickListener {
            makeRegistration()
        }

    }



    private fun makeRegistration() {

        var sellerName = etSellerName.text.toString().trim()
        var sellerUser = etSellerUser.text.toString().trim()
        var email = etSellerEmail.text.toString().trim()
        var password = etSellerPassword.text.toString().trim()
        var nif = etSellerNif.text.toString().trim()
        var cellphone1 = etSellerCellphone1.text.toString().trim()
        var cellphone2 = etSellerCellphone2.text.toString().trim()



        if (isValidate(
                sellerName,
                sellerUser,
                email,
                password,
                nif,
                cellphone1,
                cellphone2
            )
        ) {

            showDialog()
            ApiHitterHelper().registerWithSeller(sellerName,sellerUser,email,password,nif,cellphone1,
                cellphone2, this
            )
        }
    }

    private fun isValidate(
        sellerName: String,
        sellerUser: String,
        email: String,
        password: String,
        nif: String,
        cellphone1: String,
        cellphone2: String
    ): Boolean {

        if (sellerName.isEmpty() || sellerUser.isEmpty() || email.isEmpty() || password.isEmpty() || nif.isEmpty() || cellphone1.isEmpty() || cellphone2.isEmpty()) {

            toast("All field required")
            return false

        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                toast("Enter valid Email")
                return false
            } else
                return true
        }

    }
}