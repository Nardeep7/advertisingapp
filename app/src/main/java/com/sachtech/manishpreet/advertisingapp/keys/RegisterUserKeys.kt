package com.sachtech.manishpreet.advertisingapp.keys

object RegisterUserKeys{
    val REGISTER_WITH_ADMIN="register_with_admin"
    val REGISTER_WITH_SELLER="register_with_celler"
    val REGISTER_WITH_CUSTOMER="register_with_customer"
    val REGISTER_WITH_PROMTER="register_with_promoter"

}