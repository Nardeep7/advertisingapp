package com.sachtech.manishpreet.advertisingapp.extension

import com.google.gson.Gson
import com.sachtech.manishpreet.advertisingapp.app.App
import java.util.*

fun setPrefrence(key: String, value: Any) {
    val preference = App.getApp().pref
    val editor = preference.edit()

    when (value) {
        is String -> editor.putString(key, value as String)
        is Boolean -> editor.putBoolean(key, value as Boolean)
        is Long -> editor.putLong(key, value as Long)

    }
    editor.apply()
}

fun clearPrefrences() {
    val preference = App.getApp().pref
    val editor = preference.edit()
    editor.clear()
    editor.commit()
}


inline fun <reified T> getPrefrence(key: String, deafultValue: T): T {
    val preference = App.getApp().pref
    return when (T::class) {
        String::class -> preference.getString(key, deafultValue as String) as T
        Boolean::class -> preference.getBoolean(key, deafultValue as Boolean) as T
        Long::class -> preference.getLong(key, deafultValue as Long) as T
        else -> {
            " " as T
        }
    }
}

inline fun <reified T> setprefrenceObject(key: String, obj: T) {
    setPrefrence(key, Gson().toJson(obj))
}

inline fun <reified T> getprefObject(key: String): T {
    return Gson().fromJson(getPrefrence(key, ""), T::class.java)
}

fun getDateDifference(startDate: Long): Long {
    var different = Calendar.getInstance().time.time - startDate

    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24

    val days = different / daysInMilli
    different %= daysInMilli

    return days
}