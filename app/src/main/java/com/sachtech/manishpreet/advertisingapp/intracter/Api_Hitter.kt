package com.balwinderbadgal.vidpro.intracter

import com.sachtech.manishpreet.advertisingapp.constants.Constants
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


/* Created by Manish Kumar
on 26/11/18  */
object Api_Hitter {


    fun ApiInterface(): ApiInterface {

        return getRetrofit()!!.create(ApiInterface::class.java)

    }

    private fun getOkHttpClient(): OkHttpClient? {
        return OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(10000, TimeUnit.SECONDS)
            .connectTimeout(10000, TimeUnit.SECONDS)
            .build()
    }

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_API)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClient())
            .build()
    }

    interface ApiInterface {
        //https://www.googleapis.com/youtube/v3/search?part=snippet&pageToken=&relatedToVideoId=z-vggRD3ATA&type=video&order=date&maxResults=10&key=AIzaSyAbmflD_ErsJlg_dlxAbscFM-BdH7uweB0
        @GET("login/userlogin")
        fun login(
            @Query("email") email: String,
            @Query("API_KEY") key: String = "abcd",
            @Query("password") password: String
        ): Call<ResponseBody>


        @FormUrlEncoded
        @POST("login/userSignUp")
        fun registerWithAdmin(
            @Field("user_type") user_type: String=Constants.ADMIN,
            @Field("admin_name") admin_name: String,
            @Field("API_KEY") key: String = "abcd",
            @Field("admin_pwd") admin_pwd: String,
            @Field("admin_user") admin_user: String,
            @Field("admin_email") admin_email: String
        ): Call<ResponseBody>

        @FormUrlEncoded
        @POST("login/userSignUp")
        fun registerWithSeller(
            @Field("user_type") user_type: String=Constants.SELLER,
            @Field("seller_name") seller_name: String,
            @Field("API_KEY") key: String = "abcd",
            @Field("seller_user") seller_user: String,
            @Field("seller_email") seller_email: String,
            @Field("seller_nif") seller_nif: String,
            @Field("seller_cellphone") seller_cellphone: String,
            @Field("seller_telephone") seller_telephone: String,
            @Field("seller_pwd") seller_pwd: String
        ): Call<ResponseBody>


        @FormUrlEncoded
        @POST("login/userSignUp")
        fun registerWithCustomer(
            @Field("user_type") user_type: String=Constants.CUSTOMER,
            @Field("customer_name") customer_name: String,
            @Field("API_KEY") key: String = "abcd",
            @Field("customer_user") customer_user: String,
            @Field("customer_email") customer_email: String,
            @Field("customer_pwd") customer_pwd: String,
            @Field("customer_nif") customer_nif: String,
            @Field("customer_cellphone_1") customer_cellphone_1: String,
            @Field("customer_cellphone_2") customer_cellphone_2: String,
            @Field("contact_name") contact_name: String,
            @Field("customer_address") customer_address: String,
            @Field("customer_zipcode") customer_zipcode: String,
            @Field("customer_district") customer_district: String,
            @Field("customer_conselho") customer_conselho: String,
            @Field("customer_freguesia") customer_freguesia: String,
            @Field("customer_branch") customer_branch: String,
            @Field("customer_birthdate") seller_birthdate: String,
            @Field("customer_traffic") customer_traffic: String
        ): Call<ResponseBody>


        @FormUrlEncoded
        @POST("login/userSignUp")
        fun registerWithPromoter(
            @Field("user_type") user_type: String=Constants.PROMOTER,
            @Field("promoter_name") promoter_name: String,
            @Field("API_KEY") key: String = "abcd",
            @Field("promoter_user") promoter_user: String,
            @Field("promoter_email") promoter_email: String,
            @Field("promoter_pwd") promoter_pwd: String
        ): Call<ResponseBody>
    }
}