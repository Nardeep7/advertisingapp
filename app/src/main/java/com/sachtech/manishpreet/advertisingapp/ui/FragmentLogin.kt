package com.sachtech.manishpreet.advertisingapp.ui

import android.os.Bundle
import android.view.View
import com.balwinderbadgal.vidpro.intracter.ApiHitterHelper
import com.balwinderbadgal.vidpro.intracter.DataGetter
import com.google.gson.Gson
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.base.KotlinBaseFragment
import com.sachtech.manishpreet.advertisingapp.extension.*
import com.sachtech.manishpreet.advertisingapp.keys.PrefrencesKeys
import com.sachtech.manishpreet.advertisingapp.keys.RegisterUserKeys
import com.sachtech.manishpreet.advertisingapp.models.user_data.UserData
import com.sachtech.manishpreet.advertisingapp.ui.video_player.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.ResponseBody


class FragmentLogin : KotlinBaseFragment(R.layout.fragment_login), DataGetter {

    override fun onError(message: String?) {
        hideDialog()
        toast(message)
    }


    override fun onFetchData(response: ResponseBody?) {
        hideDialog()
        var userdata= Gson().fromJson(response!!.string(), UserData::class.java)
        if (userdata.isStatus)
        {
            toast(userdata.message)
            setprefrenceObject(PrefrencesKeys.LOGED_IN,true)
            setprefrenceObject(PrefrencesKeys.USER,userdata)
            context!!.openActivity<MainActivity>()
            activity!!.finish()
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        txtRegisterHere.setOnClickListener {
            openFragment()

        }
        txtLogin.setOnClickListener {
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()

            if (isvalidate(email = email, pass = password)) {
                showDialog()
                ApiHitterHelper().login(email, password, this)
            }
        }
    }

    private fun openFragment() {
        var userType = getPrefrence(PrefrencesKeys.USER_TYPE, "")
        if (userType.equals(RegisterUserKeys.REGISTER_WITH_ADMIN))
            baselistener.navigateToFragment(FragmentRegister_Admin::class)
        else if (userType.equals(RegisterUserKeys.REGISTER_WITH_SELLER))
            baselistener.navigateToFragment(FragmentRegister_Seller::class)
        else if (userType.equals(RegisterUserKeys.REGISTER_WITH_CUSTOMER))
            baselistener.navigateToFragment(FragmentRegister_Customer::class)
        else if (userType.equals(RegisterUserKeys.REGISTER_WITH_PROMTER))
            baselistener.navigateToFragment(FragmentRegister_Promoter::class)
    }

}