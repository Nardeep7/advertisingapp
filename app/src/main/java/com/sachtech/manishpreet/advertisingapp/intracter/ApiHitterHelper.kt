package com.balwinderbadgal.vidpro.intracter

import com.sachtech.manishpreet.advertisingapp.extension.toast
import com.sachtech.manishpreet.advertisingapp.models.error.ErrorBody
import com.sachtech.manishpreet.advertisingapp.ui.FragmentRegister_Seller
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response


class ApiHitterHelper() {

    var callback: Call<ResponseBody>? = null

    fun login(email: String, password: String, listner: DataGetter) {
        callback = Api_Hitter.ApiInterface().login(email = email, password = password)
        callApi(callback!!, listner)
    }

    fun callApi(
        callback: Call<ResponseBody>,
        listner: DataGetter
    ) {
        if (callback != null) {
            callback!!.enqueue(
                object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        toast(t.message!!)
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        if (response.code() == 200 || response.code() == 201) {
                            listner.onFetchData(response.body())

                        } else {
                            val converter: Converter<ResponseBody, ErrorBody> = Api_Hitter.getRetrofit()
                                .responseBodyConverter(ErrorBody::class.java, arrayOf())
                            val error = converter.convert(response.errorBody())
                            toast(error.message)
                            listner.onError(error.message ?: "")
                        }


                    }

                })

        }
    }

    fun registerWithAdmin(
        userName: String,
        adminUser: String,
        email: String,
        password: String,
        listner: DataGetter
    ) {

        callback = Api_Hitter.ApiInterface().registerWithAdmin(
            admin_name = userName,
            admin_user = adminUser,
            admin_email = email,
            admin_pwd = password)
        callApi(callback!!, listner)
    }


    fun registerWithSeller(
        sellerName: String,
        sellerUser: String,
        email: String,
        password: String,
        nif: String,
        cellphone1: String,
        cellphone2: String,
        listner: DataGetter
    ) {

        callback = Api_Hitter.ApiInterface().registerWithSeller(
            seller_name = sellerName,seller_user = sellerUser,seller_email = email,seller_pwd = password,seller_nif = nif,
            seller_cellphone = cellphone1,seller_telephone = cellphone2)
        callApi(callback!!, listner)
    }


    fun registerWithCustomer(
        customerName: String,
        customerUser: String,
        email: String,
        password: String,
        nif: String,
        cellphone1: String,
        cellphone2: String,
        contactName: String,
        customerAddress: String,
        customerZipcode: String,
        customerDistrict: String,
        customerConselho: String,
        customerFreegsia: String,
        customerBranch: String,
        customerTraffic: String,
        customerBirthdate:String,
        listner: DataGetter
    ) {

        callback = Api_Hitter.ApiInterface().registerWithCustomer(customer_name = customerName,customer_user = customerUser,customer_email = email,
            customer_pwd = password,customer_nif = nif,customer_cellphone_1 = cellphone1,customer_cellphone_2 = cellphone2,contact_name = contactName,
            customer_address = customerAddress,customer_zipcode = customerZipcode,customer_district = customerDistrict,customer_conselho = customerConselho,
            customer_freguesia = customerFreegsia,customer_branch = customerBranch,seller_birthdate = customerBirthdate,customer_traffic = customerTraffic)
        callApi(callback!!, listner)
    }


    fun registerWithPromoter(
        userName: String,
        promoterUser: String,
        email: String,
        password: String,
        listner: DataGetter
    ) {

        callback = Api_Hitter.ApiInterface().registerWithPromoter(
            promoter_name = userName,
            promoter_user = promoterUser,
            promoter_email = email,
            promoter_pwd = password)
        callApi(callback!!, listner)
    }

}





