package com.sachtech.manishpreet.advertisingapp.ui

import android.os.Bundle
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.base.KotlinBaseActivity

class HomeActivity : KotlinBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home2)
    }
}
