package com.sachtech.manishpreet.advertisingapp.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.extension.openActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            openActivity<SelectionActivity>()
            finish()

        },3000)
    }
}
