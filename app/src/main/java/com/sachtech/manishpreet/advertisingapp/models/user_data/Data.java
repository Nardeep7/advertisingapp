package com.sachtech.manishpreet.advertisingapp.models.user_data;

import com.google.gson.annotations.SerializedName;


public class Data {

    @SerializedName("updated_on")
    private String updatedOn;

    @SerializedName("contact_name")
    private String contactName;

    @SerializedName("address")
    private String address;

    @SerializedName("birth_date")
    private String birthDate;

    @SerializedName("Traffic")
    private String traffic;

    @SerializedName("nif")
    private String nif;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("zip_code")
    private String zipCode;

    @SerializedName("cell_phone")
    private String cellPhone;

    @SerializedName("password")
    private String password;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("created_on")
    private String createdOn;

    @SerializedName("Branch")
    private String branch;

    @SerializedName("district")
    private String district;

    @SerializedName("name")
    private String name;

    @SerializedName("Freguesia")
    private String freguesia;

    @SerializedName("user")
    private String user;

    @SerializedName("email")
    private String email;

    @SerializedName("conselho")
    private String conselho;

    @SerializedName("status")
    private String status;

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }

    public String getTraffic() {
        return traffic;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNif() {
        return nif;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranch() {
        return branch;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return district;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFreguesia(String freguesia) {
        this.freguesia = freguesia;
    }

    public String getFreguesia() {
        return freguesia;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setConselho(String conselho) {
        this.conselho = conselho;
    }

    public String getConselho() {
        return conselho;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "Data{" +
                        "updated_on = '" + updatedOn + '\'' +
                        ",contact_name = '" + contactName + '\'' +
                        ",address = '" + address + '\'' +
                        ",birth_date = '" + birthDate + '\'' +
                        ",traffic = '" + traffic + '\'' +
                        ",nif = '" + nif + '\'' +
                        ",telephone = '" + telephone + '\'' +
                        ",zip_code = '" + zipCode + '\'' +
                        ",cell_phone = '" + cellPhone + '\'' +
                        ",password = '" + password + '\'' +
                        ",user_type = '" + userType + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",created_on = '" + createdOn + '\'' +
                        ",branch = '" + branch + '\'' +
                        ",district = '" + district + '\'' +
                        ",name = '" + name + '\'' +
                        ",freguesia = '" + freguesia + '\'' +
                        ",user = '" + user + '\'' +
                        ",email = '" + email + '\'' +
                        ",conselho = '" + conselho + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}