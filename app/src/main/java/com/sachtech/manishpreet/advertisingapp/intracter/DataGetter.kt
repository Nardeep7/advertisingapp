package com.balwinderbadgal.vidpro.intracter

import okhttp3.ResponseBody

interface DataGetter {
    fun onFetchData(body: ResponseBody?)
    fun onError(message:String?)

}