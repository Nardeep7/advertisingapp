package com.sachtech.manishpreet.advertisingapp.ui

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.extension.*
import com.sachtech.manishpreet.advertisingapp.keys.PrefrencesKeys
import com.sachtech.manishpreet.advertisingapp.keys.RegisterUserKeys
import kotlinx.android.synthetic.main.activity_selection.*

class SelectionActivity : AppCompatActivity(), View.OnClickListener {

    var isSelected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selection)


        //setlistner for views
        setListner()
    }


    private fun setListner() {

        layoutAdmin.setOnClickListener(this)
        layoutSeller.setOnClickListener(this)
        layoutCustomer.setOnClickListener(this)
        layoutPromoter.setOnClickListener(this)
        txtNext.setOnClickListener(this)
    }


    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.layoutAdmin -> setLayout(
                RegisterUserKeys.REGISTER_WITH_ADMIN,
                layoutAdmin,
                ivAdmin,
                layoutSeller,
                ivSeller,
                layoutCustomer,
                ivCustomer,
                layoutPromoter,
                ivPromoter
            )
            R.id.layoutSeller -> setLayout(
                RegisterUserKeys.REGISTER_WITH_SELLER,
                layoutSeller,
                ivSeller,
                layoutAdmin,
                ivAdmin,
                layoutCustomer,
                ivCustomer,
                layoutPromoter,
                ivPromoter
            )
            R.id.layoutCustomer -> setLayout(
                RegisterUserKeys.REGISTER_WITH_CUSTOMER,
                layoutCustomer,
                ivCustomer,
                layoutSeller,
                ivSeller,
                layoutAdmin,
                ivAdmin,
                layoutPromoter,
                ivPromoter
            )
            R.id.layoutPromoter -> setLayout(
                RegisterUserKeys.REGISTER_WITH_PROMTER,
                layoutPromoter,
                ivPromoter,
                layoutSeller,
                ivSeller,
                layoutCustomer,
                ivCustomer,
                layoutAdmin,
                ivAdmin
            )
            R.id.txtNext -> {
                var permission = arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                )

                if (hasPermissions(permission)) {
                    if (isSelected)
                        openActivity<LoginActivity>()
                    else
                        toast("select user type")

                } else {
                    permission.withPermissions(this)
                }

        }
    }
}

private fun setLayout(
    user_type: String,
    activeView: LinearLayout,
    activeImage: ImageView,
    inactive_one: LinearLayout,
    inactiveImageOne: ImageView,
    inactive_two: LinearLayout,
    inactiveImageTwo: ImageView,
    inactive_three: LinearLayout,
    inactiveImageThree: ImageView
) {
    isSelected = true
    setPrefrence(PrefrencesKeys.USER_TYPE, user_type)
    activeView.background = resources.getDrawable(R.drawable.corner_background)
    activeImage.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN)

    inactive_one.background = resources.getDrawable(R.drawable.white_background)
    inactiveImageOne.setColorFilter(
        ContextCompat.getColor(this, R.color.colorPrimary),
        android.graphics.PorterDuff.Mode.SRC_IN
    )
    inactive_two.background = resources.getDrawable(R.drawable.white_background)
    inactiveImageTwo.setColorFilter(
        ContextCompat.getColor(this, R.color.colorPrimary),
        android.graphics.PorterDuff.Mode.SRC_IN
    )
    inactive_three.background = resources.getDrawable(R.drawable.white_background)
    inactiveImageThree.setColorFilter(
        ContextCompat.getColor(this, R.color.colorPrimary),
        android.graphics.PorterDuff.Mode.SRC_IN
    )

}

}
