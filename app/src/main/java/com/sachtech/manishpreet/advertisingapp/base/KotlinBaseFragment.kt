package com.sachtech.manishpreet.advertisingapp.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast

/**
 * * Created by Gurtek Singh on 1/1/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */
abstract class KotlinBaseFragment(@LayoutRes val view: Int = 0) : Fragment() {

    protected lateinit var baselistener: KotlinBaseListener
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is KotlinBaseListener) {
            baselistener = context
        } else {
            throw IllegalStateException("You Must have to extends your activity with KotlinBaseActivity")
        }
    }


    fun shortToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    protected fun isAppInstalled(packageName: String): Boolean {
        val mIntent = activity?.getPackageManager()
            ?.getLaunchIntentForPackage(packageName)
        return mIntent != null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(view, container, false)
    }


    fun addDialog(resId: Int): Dialog {
        val d = Dialog(activity)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        d.setCancelable(false)
        d.setContentView(resId)
        d.window!!
            .setBackgroundDrawableResource(android.R.color.transparent)
        return d

    }

    fun addDialogCancelable(resId: Int): Dialog {
        val d = Dialog(activity)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        d.setCancelable(true)
        d.setContentView(resId)
        d.window!!
            .setBackgroundDrawableResource(android.R.color.transparent)
        return d

    }

    fun <T> showDialog(clazz: Class<out KotlinBaseDialogFragment<T>>, bundle: Bundle? = Bundle()): Fragment? {
        val tag = clazz.simpleName
        val ft = fragmentManager?.beginTransaction()
        var fragment = fragmentManager?.findFragmentByTag(tag)
        if (fragment != null) {
            ft?.remove(fragment)
        }
        ft?.addToBackStack(tag)

        // Create and show the dialog.
        fragment = clazz.newInstance()
        fragment.arguments = bundle
        fragment.show(ft, tag)

        return fragment
    }

    fun showDialog() {
        if (activity != null)
            (activity as KotlinBaseActivity).showDialog()
    }

    fun hideDialog() {
        if (activity != null)
            (activity as KotlinBaseActivity).hideDialog()
    }

    fun showMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

}