package com.sachtech.manishpreet.advertisingapp.constants

object Constants {
    val ADMIN = "admin"
    val SELLER = "seller"
    val CUSTOMER = "customer"
    val PROMOTER = "promoter"

    val BASE_API="http://carreguefacil.datatecnologias.com/advertising_app/api/"
}
