package com.sachtech.manishpreet.advertisingapp.ui

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.balwinderbadgal.vidpro.intracter.ApiHitterHelper
import com.balwinderbadgal.vidpro.intracter.DataGetter
import com.google.gson.Gson
import com.sachtech.manishpreet.advertisingapp.R
import com.sachtech.manishpreet.advertisingapp.base.KotlinBaseFragment
import com.sachtech.manishpreet.advertisingapp.extension.openActivity
import com.sachtech.manishpreet.advertisingapp.extension.setprefrenceObject
import com.sachtech.manishpreet.advertisingapp.extension.toast
import com.sachtech.manishpreet.advertisingapp.keys.PrefrencesKeys
import com.sachtech.manishpreet.advertisingapp.models.user_data.UserData
import com.sachtech.manishpreet.advertisingapp.ui.video_player.MainActivity
import kotlinx.android.synthetic.main.fragment_register_admin.*
import okhttp3.ResponseBody

class FragmentRegister_Admin : KotlinBaseFragment(R.layout.fragment_register_admin), DataGetter {

    override fun onError(message: String?) {
        hideDialog()
        toast(message)
    }

    override fun onFetchData(body: ResponseBody?) {
        hideDialog()
        var userdata=Gson().fromJson(body!!.string(), UserData::class.java)
        if (userdata.isStatus)
        {
            toast(userdata.message)
            setprefrenceObject(PrefrencesKeys.LOGED_IN,true)
            setprefrenceObject(PrefrencesKeys.USER,userdata)
            context!!.openActivity<MainActivity>()
            activity!!.finish()
        }else{
            toast(userdata.message)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtAlreadyHaveAccount.setOnClickListener {
            baselistener.navigateToFragment(FragmentLogin::class)
        }
        txtRegister.setOnClickListener {
            makeRegistration()
        }

    }
    private fun makeRegistration() {

        var userName=etName.text.toString().trim()
        var adminUser=etAdminUser.text.toString().trim()
        var email=etEmail.text.toString().trim()
        var password=etPassword.text.toString().trim()

        if (isValidate(userName,adminUser,email,password))
        {
            showDialog()
          ApiHitterHelper().registerWithAdmin(userName,adminUser,email,password,this)
        }
    }

    private fun isValidate(
        userName: String,
        adminUser: String,
        email: String,
        password: String
    ): Boolean {
        if (userName.isEmpty()||adminUser.isEmpty()||email.isEmpty()||password.isEmpty()) {
            toast("All field required")
            return false
        }
        else{
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            {
                toast("Enter valid Email")
                return false
            }else
                return true
        }

    }

}